package de.ghse.zeitplaner.resources;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Entry extends CalendarComponent {

	// Begin attributes
	protected String uid;
	protected LocalDateTime dtstamp;
	protected String cClass;
	protected String created;
	protected String description;
	protected LocalDateTime dtstart;
	protected String geo;
	protected String location;
	protected String organizer;
	protected String priority;
	protected String recurid;
	protected String seq;
	protected String status;
	protected String summary;
	protected String duration;
	protected LocalDateTime dtend;
	// End attributes

	public Entry(){
		setSummary("Neuer Eintrag");
		setDtstamp(LocalDateTime.now());
	}
	
	public Entry(LocalDateTime pTimeStart, LocalDateTime pTimeFinish){
		this();
		setDtstart(pTimeStart);
		setDtend(pTimeFinish);
		init();
	}
	
	public Entry(LocalDateTime pTimeStart, String pDuration){
		this();
		setDtstart(pTimeStart);
		setDuration(pDuration);
		init();
	}
	
	public Entry(LocalDateTime pTimeStart, String pDuration, LocalDateTime pTimestamp){
		this(pTimeStart, pDuration);
		setDtstamp(pTimestamp);
		init();
	}
	
	public Entry(LocalDateTime pTimeStart, LocalDateTime pTimeFinish, LocalDateTime pTimestamp){
		this(pTimeStart, pTimeFinish);
		setDtstamp(pTimestamp);
		init();
	}
	
	// Begin methods
	private void init() {
		uid = "standardID";
	}
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public LocalDateTime getDtstamp() {
		return dtstamp;
	}

	public void setDtstamp(LocalDateTime dtstamp) {
		this.dtstamp = dtstamp;
	}

	public String getcClass() {
		return cClass;
	}

	public void setClass(String pClass) {
		cClass = pClass;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getDtstart() {
		return dtstart;
	}

	public void setDtstart(LocalDateTime dtstart) {
		this.dtstart = dtstart;
	}

	public String getGeo() {
		return geo;
	}

	public void setGeo(String geo) {
		this.geo = geo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getRecurid() {
		return recurid;
	}

	public void setRecurid(String recurid) {
		this.recurid = recurid;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public void setDtend(LocalDateTime pEnd){
		dtend = pEnd;
	}
	
	public LocalDateTime getDtend(){
		return dtend;
	}
	
	public String ldt2ictString(LocalDateTime pLdt, boolean pToUTC) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

		ZoneId natId = ZoneId.systemDefault();
		ZonedDateTime zdtNat = ZonedDateTime.of(pLdt, natId);

		ZonedDateTime zdtConverted;

		if (pToUTC == true) {
			ZoneId utcId = ZoneId.of("Z");
			zdtConverted = zdtNat.withZoneSameInstant(utcId);
		}

		else {
			zdtConverted = zdtNat;
		}

		String formatted = zdtConverted.format(dtf);

		String date = formatted.substring(0, 8);
		String time = formatted.substring(8);

		String iCalTimeString = date + "T" + time + "Z";

		return iCalTimeString;
	}
	// End Methods
	
} // end of Entry

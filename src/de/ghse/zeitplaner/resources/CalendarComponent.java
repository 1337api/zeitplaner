package de.ghse.zeitplaner.resources;

import java.time.LocalDateTime;

public class CalendarComponent {

	// Begin attributes
	private String url;
	private LocalDateTime last_mod;
	private int calID;
	// End attributes

	// Begin methods
	public String getUrl() {
		return url;
	}

	public LocalDateTime getLast_mod() {
		return last_mod;
	}

	public void setLast_mod(LocalDateTime last_mod) {
		this.last_mod = last_mod;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	// end methods

}

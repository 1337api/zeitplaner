package de.ghse.zeitplaner.userinterfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.ghse.zeitplaner.Gui;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JToggleButton;
import javax.swing.JRadioButton;

public class OptionWindow extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Gui gui;
	private JComboBox<String> comboBox;

	/**
	 * Launch the application.
	 */
	public void initialize() {
		try {
	
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public OptionWindow(Gui pGui) {
		gui = pGui;
		this.setTitle("Optionen");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Sprache ausw\u00E4hlen", "Deutsch ", "English", "\u0985\u09B8\u09AE\u09C0\u09AF\u09BC\u09BE", "M\u00ECng-d\u0115\u0324ng-ng\u1E73\u0304", "\u10E5\u10D0\u10E0\u10D7\u10E3\u10DA\u10D8"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(10, 11, 166, 40);
		contentPanel.add(comboBox);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Benachrichtigungen ein");
		rdbtnNewRadioButton.setBounds(10, 58, 166, 40);
		contentPanel.add(rdbtnNewRadioButton);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						gui.closeOptionWindow();
						String language = (String) comboBox.getSelectedItem();
						switch(language){
						case "Deutsch": gui.setLanguage(language);
						break;
						case"English": gui.setLanguage(language);
						break;
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}

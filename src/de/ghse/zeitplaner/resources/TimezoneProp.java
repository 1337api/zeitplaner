package de.ghse.zeitplaner.resources;

import java.time.LocalDateTime;

public class TimezoneProp {
	  
	  // Begin attributes
	  private String dtstart;
	  private String tzoffsetto;
	  private String ztoffsetfrom;
	  private String tzname;
	  // End attributes
	  
	  public TimezoneProp() {
	    this.dtstart = null;
	    this.tzoffsetto = "";
	    this.ztoffsetfrom = null;
	  }

	  // Begin methods
	  public String getDtstart() {
	    return dtstart;
	  }

	  public void setDtstart(String dtstart) {
	    this.dtstart = dtstart;
	  }

	  public String getTzoffsetto() {
	    return tzoffsetto;
	  }

	  public void setTzoffsetto(String tzoffsetto) {
	    this.tzoffsetto = tzoffsetto;
	  }

	  public String getZtoffsetfrom() {
	    return ztoffsetfrom;
	  }

	  public void setZtoffsetfrom(String ztoffsetfrom) {
	    this.ztoffsetfrom = ztoffsetfrom;
	  }
	  
	  public void setTZName(String pTZName) {
		    this.tzname = pTZName;
	  }
	  
	  public String getTZName() {
		    return tzname;
	  }
	  // End methods
	  
	} // end of Timezoneprop


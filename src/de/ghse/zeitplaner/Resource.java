package de.ghse.zeitplaner;

import de.ghse.zeitplaner.resources.Calendar;
import de.ghse.zeitplaner.resources.ICalAppointment;
import de.ghse.zeitplaner.resources.ICalTask;
import de.ghse.zeitplaner.sql.DB_Viewer;

public class Resource {
	// Constructors
	public Resource(DB_Viewer pDbv) {
		cals = new Calendar[1];
		indexCals = 0;
	}

	// Attributes
	private Calendar[] cals;
	private int indexCals;
	
	// Methods
	public int addCal(Calendar pCal) {
		//add into Array
		if (cals.length <= indexCals) {
			Calendar[] temp = new Calendar[cals.length + 5];
			for (int i = 0; i < cals.length; i++) { 
				temp[i] = cals[i];
			}
			cals = temp;
		}
		cals[indexCals] = pCal;
		indexCals++;
		return pCal.getID();
	}

	public void addAppointment(int pID, ICalAppointment pAppo) {
		getCalByID(pID).addEvent(pAppo);
	}
	
	public void addAppointment(Calendar pCal, ICalAppointment pEvent) {
		pCal.addEvent(pEvent);
	}
	
	public void addTask(int pID, ICalTask pTask) {
		getCalByID(pID).addTask(pTask);
	}
	
	public void addTask(Calendar pCal, ICalTask pTask) {
		pCal.addTask(pTask);
	}
	
	public Calendar getCalByID(int pID) {
		Calendar cal = null;
		int id = 0;
		for (int i = 0; i < indexCals; i++) {
			id = cals[i].getID();
			if (id == pID) {
				cal = cals[i];
				break;
			}
		}
		return cal;
	}
	
	public Calendar getCalByName(String pName) {
		Calendar cal = null;
		String calname;
		for (int i = 0; i < indexCals; i++) {
			calname = cals[i].getName();
			if (calname == pName) {
				cal = cals[i];
				break;
			}
		}
		return cal;
	}
	
	public void delEvent(int pIdEvent, int pIdCal) {
		//TODO
		
	}
	
	public void printAllAppos() {
		System.out.println("---Printing all Appointments---");
		for(int i = 0; i < indexCals; i++) {
			System.out.println(cals[i].getName());
			System.out.println("---");
		}
		System.out.println("Finished Printing---");
	}

	public Calendar[] getCals() {
		int lenght = cals.length;
		int count = 0;
		for (int i = 0; i < lenght; i++) {
			if (cals[i] != null) {
				count++;
			}
		}
		Calendar[] temp = new Calendar[count];
		int lenghtTemp = temp.length;
		int j = 0;
		for (int i = 0; i < lenght; i++) {
			if (cals[i] != null) {
				temp[j] = cals[i];
				j++;
			}
		}	
		cals = temp;
		return cals;
	}

	public void setCals(Calendar[] pCals) {
		int lenght = pCals.length;
		for (int i = 0; i < lenght; i++) {
			addCal(pCals[i]);
		}
	}

	public void delCal(Calendar pCal) {
		int lenght = cals.length;
		for (int i = 0; i < lenght; i++) {
			if (cals[i] == pCal) {
				cals[i] = null;
				break;
			}
		}
	}
	
}

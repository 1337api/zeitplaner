package de.ghse.zeitplaner.resources;

import java.time.LocalDateTime;

public class Calendar {
	//Attributes
	private String name;
	private static int calendarID = 0;
	private int calID;
	private String prodID;
	private ICalAppointment[] appos = new ICalAppointment[0];
	private ICalTask[] tasks = new ICalTask[0];
	private int indexEvents = 0;
	private int indexTasks = 0;
	private Timezone timezone;
	
	private String calscale;
	private String method;
	
	public Calendar(){
		prodID = "-//JMC-GMBH//NONSGML OP-Kalender V1.1//EN";
		timezone = new Timezone();
		calID = calendarID;
		calendarID++;
	}
	
	public Calendar(String pName){
		this();
		name = pName;
	}
	
	public void setTimezone(Timezone pTimezone){
		timezone = pTimezone;
	}
	
	public void addEvent(ICalAppointment pAppo) {
		pAppo.setCalName(this.getName());
		if (appos.length <= indexEvents) {
			ICalAppointment[] temp = new ICalAppointment[appos.length + 1];

			for (int i = 0; i < appos.length; i++) {
				temp[i] = appos[i];
			}
			appos = temp;
		}
		appos[indexEvents] = pAppo;
		indexEvents++;
	}
	
	public void addTask(ICalTask pTask) {
		pTask.setCalName(this.getName());
		if(tasks.length <= indexTasks) {
			ICalTask[] temp = new ICalTask[tasks.length + 1];

			for (int i = 0; i < tasks.length; i++) {
				temp[i] = tasks[i];
			}
			tasks = temp;
		}
		tasks[indexTasks] = pTask;
		indexTasks++;
	}
	
	public String[] getICS() {
		String[] icsString = new String[100];;
		//TODO move this method to control?
		//ics vCalendar Head
		icsString[0] = "BEGIN:VCALENDAR"+ "\r";
		icsString[1] = "VERSION:2.0"+ "\r";
		icsString[2] = "PRODID:-/ghse/zeitplaner/9/11/was an inside job\\-"+ "\r";
		if (calscale != null) {										//Additional Attribute
			icsString[3] = "CALSCALE:" +calscale+ "\r";
		}
		if (method != null) {										//Additional Attribute
			icsString[4] = "METHOD:"+method + "\r";
		}
		//ics vCalendar Body
		String[] allEntries = getICSComponent();
		//TODO Reading Components
		//ics vCalendar Feet
		int counter = 0;
		for(int i=0;i<allEntries.length;i++){
			if(allEntries[i] != null){
				icsString[5+i] = allEntries[i];
				counter++;
			}
			else{
			}
			
		}
		icsString[5+counter] = "END:VCALENDAR"+ "\r";
		return icsString;
	}
	
	private String[] getICSComponent(){
		//String compString = "Alle Komponenten"+ "\r";
		String[] entryString = new String[100];
		int firstIndex = 0;
		for(int i = 0; i<indexTasks; i++){
			String taskString[] = tasks[i].generateICS();
			for(int k=0; k<taskString.length; k++){
				entryString[k] = taskString[k];
			}
			firstIndex=i;
		}
		for(int i = firstIndex; i<indexEvents; i++){
			String eventString[] = appos[i].generateICS();
			for(int k=0; k<eventString.length; k++){
				entryString[k] = eventString[k];
			}
		}
		//TODO get all Components
		return entryString;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String pName){
		name = pName;
	}
	
	public void setID(int pID) {
		calID = pID;
	}

	public int getID() {
		return calID;
	}

	public String getCalscale() {
		return calscale;
	}
	
	public void setCalscale(String pCalscale) {
		calscale = pCalscale;
	}

	public String getMethod() {
		return method;
	}
	
	public void setMethod(String pMethod) {
		method = pMethod;
	}

	public int getIndexEvents() {
		return indexEvents;
	}

	public ICalAppointment getEvent(int pIndex) {
		if (pIndex >= 0 && pIndex <= indexEvents) {
			return appos[pIndex];
		} else {
			return null;
		}
	}
	
	public int getIndexTask(){
		return indexTasks;
	}
	
	public ICalTask getTask(int pIndex) {
		if (pIndex >= 0 && pIndex <= indexTasks) {
			return tasks[pIndex];
		} else {
			return null;
		}
	}
	
	public void print(){
		System.out.println();
		System.out.println("Eigenschaften des Kalenders: ");
		System.out.print("Name:");
		if(name!=null){
			System.out.println(name);
		}
		else{
			System.out.println("-");
		}
		
		System.out.print("ProduzentenID:");
		if(prodID!=null){
			System.out.println(prodID);
		}
		else{
			System.out.println("-");
		}
		
		System.out.print("Kalenderskala:");
		if(calscale!=null){
			System.out.println(calscale);
		}
		else{
			System.out.println("-");
		}
		
		System.out.print("Methode:");
		if(method!=null){
			System.out.println(method);
		}
		else{
			System.out.println("-");
		}
		System.out.println();
		
		System.out.println("Die Termine:");
		for (int i = 0; i < appos.length; i++) {
			System.out.println(i+1 + ".Termin:");
			
			System.out.print("Name: ");
			if (appos[i].getSummary() != null)
				System.out.println(appos[i].getSummary());
			else{
				System.out.println("-");
			}
			
			System.out.print("Ort: ");
			if (appos[i].getLocation() != null)
				System.out.println(appos[i].getLocation());
			else{
				System.out.println("-");
			}
			
			System.out.print("Beschreibung: ");
			if (appos[i].getDescription() != null)
				System.out.println(appos[i].getDescription());
			else{
				System.out.println("-");
			}
			
			System.out.print("Von: ");
			if (appos[i].getDtstart() != null)
				System.out.println(appos[i].getDtstart());
			else{
				System.out.println("-");
			}
			
			System.out.print("Bis: ");
			if (appos[i].getDtend() != null)
				System.out.println(appos[i].getDtend());
			else{
				System.out.println("-");
			}
			
			System.out.print("Dauer: ");
			if (appos[i].getDuration() != null)
				System.out.println(appos[i].getDuration());
			else{
				System.out.println("-");
			}
			
			System.out.print("Zeitstempel: ");
			if (appos[i].getDtstamp() != null)
				System.out.println(appos[i].getDtstamp());
			else{
				System.out.println("-");
			}
			
			System.out.print("UID:");
			if (appos[i].getUid() != null)
				System.out.println(appos[i].getUid());
			else{
				System.out.println("-");
			}
			
			System.out.print("Zugriff:");
			if (appos[i].getcClass() != null)
				System.out.println(appos[i].getcClass());
			else{
				System.out.println("-");
			}
			
			System.out.print("Erstellt:");
			if (appos[i].getCreated() != null)
				System.out.println(appos[i].getCreated());
			else{
				System.out.println("-");
			}
			
			System.out.print("Koordinaten:");
			if (appos[i].getGeo() != null)
				System.out.println(appos[i].getGeo());
			else{
				System.out.println("-");
			}

			System.out.print("Organisateur:");
			if (appos[i].getOrganizer() != null)
				System.out.println(appos[i].getOrganizer());
			else{
				System.out.println("-");
			}

			System.out.print("Prioritšt:");
			if (appos[i].getPriority() != null)
				System.out.println(appos[i].getPriority());
			else{
				System.out.println("-");
			}

			System.out.print("RecurID:");
			if (appos[i].getRecurid() != null)
				System.out.println(appos[i].getRecurid());
			else{
				System.out.println("-");
			}

			System.out.print("Status:");
			if (appos[i].getStatus() != null)
				System.out.println(appos[i].getStatus());
			else{
				System.out.println("-");
			}
			
			System.out.print("Transparenz:");
			if (appos[i].getTransp() != null)
				System.out.println(appos[i].getTransp());
			else{
				System.out.println("-");
			}
			
			System.out.print("URL:");
			if (appos[i].getUrl() != null)
				System.out.println(appos[i].getUrl());
			else{
				System.out.println("-");
			}
		}
		System.out.println();
		System.out.println();
		
		System.out.println("Die Aufgaben:");
		System.out.println();
		for(int i = 0; i<tasks.length; i++){
			System.out.println(i+1 + ".Aufgabe:");
			
			System.out.print("Name: ");
			if (tasks[i].getSummary() != null)
				System.out.println(tasks[i].getSummary());
			else{
				System.out.println("-");
			}
			
			System.out.print("Ort: ");
			if (tasks[i].getLocation() != null)
				System.out.println(tasks[i].getLocation());
			else{
				System.out.println("-");
			}
			
			System.out.print("Beschreibung: ");
			if (tasks[i].getDescription() != null)
				System.out.println(tasks[i].getDescription());
			else{
				System.out.println("-");
			}
			
			System.out.print("Von: ");
			if (tasks[i].getDtstart() != null)
				System.out.println(tasks[i].getDtstart());
			else{
				System.out.println("-");
			}
			
			System.out.print("Bis: ");
			if (tasks[i].getDtend() != null)
				System.out.println(tasks[i].getDtend());
			else{
				System.out.println("-");
			}
			
			System.out.print("Dauer: ");
			if (tasks[i].getDuration() != null)
				System.out.println(tasks[i].getDuration());
			else{
				System.out.println("-");
			}
			
			System.out.print("Zeitstempel: ");
			if (tasks[i].getDtstamp() != null)
				System.out.println(tasks[i].getDtstamp());
			else{
				System.out.println("-");
			}
			
			System.out.print("UID:");
			if (tasks[i].getUid() != null)
				System.out.println(tasks[i].getUid());
			else{
				System.out.println("-");
			}
			
			System.out.print("Zugriff:");
			if (tasks[i].getcClass() != null)
				System.out.println(tasks[i].getcClass());
			else{
				System.out.println("-");
			}
			
			System.out.print("Erstellt:");
			if (tasks[i].getCreated() != null)
				System.out.println(tasks[i].getCreated());
			else{
				System.out.println("-");
			}
			
			System.out.print("Koordinaten:");
			if (tasks[i].getGeo() != null)
				System.out.println(tasks[i].getGeo());
			else{
				System.out.println("-");
			}

			System.out.print("Organisateur:");
			if (tasks[i].getOrganizer() != null)
				System.out.println(tasks[i].getOrganizer());
			else{
				System.out.println("-");
			}

			System.out.print("Prioritšt:");
			if (tasks[i].getPriority() != null)
				System.out.println(tasks[i].getPriority());
			else{
				System.out.println("-");
			}

			System.out.print("RecurID:");
			if (tasks[i].getRecurid() != null)
				System.out.println(tasks[i].getRecurid());
			else{
				System.out.println("-");
			}

			System.out.print("Status:");
			if (tasks[i].getStatus() != null)
				System.out.println(tasks[i].getStatus());
			else{
				System.out.println("-");
			}
			
			System.out.print("Abgeschlossen:");
			if (tasks[i].getCompleted() != null)
				System.out.println(tasks[i].getCompleted());
			else{
				System.out.println("-");
			}
			
			System.out.print("Ende:");
			if (tasks[i].getDue() != null)
				System.out.println(tasks[i].getDue());
			else{
				System.out.println("-");
			}
		}
	}


}
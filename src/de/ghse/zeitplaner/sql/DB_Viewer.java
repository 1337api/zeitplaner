package de.ghse.zeitplaner.sql;

import java.sql.*;

import de.ghse.zeitplaner.resources.Calendar;
import de.ghse.zeitplaner.resources.ICalAppointment;

public class DB_Viewer {
	//Constructors
	public DB_Viewer(String pDatabaseName) {
		openDB(pDatabaseName);
	}
	
	//Attributes
	private Connection conn;
	private String userName;
	private String password;
	private String url;
	
	//Methodes
	public void openDB(String pDatabaseName) {
		userName = "root";
		password = null;
		url = "jdbc:mysql://localhost/" + pDatabaseName;
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database Connection established ...");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		} catch (Exception ex) {
			System.out.println("Cannot connect to database server");
			conn = null;
		}
		
	}
	
	public ResultSet execSQL(String pQuery) {
		Statement stmt;
		ResultSet rs = null;
		
		try {
			stmt = conn.createStatement();
			if (stmt.execute(pQuery + ";")) {
				rs = stmt.getResultSet();
			}
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		} catch (Exception ex) {
			System.out.println("Failed to execude Query (" + ex.getMessage() + ")");
		}
		
		return rs;
	}

	public void addCal(Calendar pCal) {
		try {
			execSQL("insert into Calendars (calscale, method) value(" +	pCal.getCalscale() + ", " +	pCal.getMethod() + ")");
			ResultSet rs = execSQL("SELECT max(c.id) AS ID FROM calendars c");
			rs.first();
			pCal.setID(Integer.parseInt(rs.getString("ID")));
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage() + "\n while adding Calendar ");
			pCal.setID(0);
		} catch (Exception ex) {
			System.out.println("SQL command failed (" + ex.getMessage() + ")");
			pCal.setID(0);
		}
	}

	public void removeCal(Calendar pCal) {
		int id = pCal.getID();
		try {
			execSQL("DELETE FROM calendars WHERE id = " + Integer.toString(pCal.getID()));
			pCal.setID(0);
		} catch (Exception ex) {
			System.out.println("SQL command failed (" + ex.getMessage() + ")\n while removing Calendar");
			pCal.setID(id);
		}
	}

	public void addEvent(int calID, ICalAppointment pEvent) {
		// TODO 
	}

	public Calendar[] getCalendars() {
		int countCals = 0;
		try {
			ResultSet rs = execSQL("SELECT count(c.id) FROM calendars c");
			rs.first();
			countCals=Integer.parseInt(rs.getString(1));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Calendar[] cals = new Calendar[countCals];
		
		try {
			ResultSet rs = execSQL("SELECT * FROM calendars c");
			rs.beforeFirst();
			for (int i = 0; i < countCals; i++) {
				rs.next();
				
				cals[i] = new Calendar();
				cals[i].setID(Integer.parseInt(rs.getString("id")));
				cals[i].setCalscale(rs.getString("calscale"));
				cals[i].setMethod(rs.getString("method"));
			}		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return cals;
	}
	
	
}

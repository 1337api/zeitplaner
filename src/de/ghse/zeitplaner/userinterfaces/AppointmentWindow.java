package de.ghse.zeitplaner.userinterfaces;

import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import de.ghse.zeitplaner.Gui;
import de.ghse.zeitplaner.resources.Calendar;
import de.ghse.zeitplaner.resources.ICalAppointment;

import javax.swing.JTextArea;
import java.awt.TextArea;
import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Label;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.JMenu;

public class AppointmentWindow extends JDialog {
	private String[] namenCal = {"Name"};
	private String[] namenAppo = {"Name", "Von", "Bis", "Ort"};
	private TableModel jtModelC;
	private TableModel jtModelA;
	private Gui gui;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField_Name;
	private JTextField textField_Location;
	private JRadioButton rdbtnKalender;
	private JRadioButton rdbtnTermin;
	private JRadioButton rdbtnAufgabe;
	private JTextField textField_YearFrom;
	private JTextField textField_MonthFrom;
	private JTextField textField_DayFrom;
	private JTextField textField_HourFrom;
	private JTextField textField_MinuteFrom;
	private JTextField textField_YearTo;
	private JTextField textField_MonthTo;
	private JTextField textField_DayTo;
	private JTextField textField_HourTo;
	private JTextField textField_MinuteTo;
	private JComboBox cbCalendars;
	private JTextField textField_Description;
	private int indexC = 0;
	private int indexA = 0;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public void initialize() {
		try {
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AppointmentWindow(Gui pGui) {
		setTitle("Termin/Aufgabe/Kalender erstellen");
		gui = pGui;
		setBounds(100, 100, 456, 508);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						gui.closeAppoWindow();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			contentPanel.setLayout(null);
			
			JLabel lblNameAppo = new JLabel("Titel");
			lblNameAppo.setBounds(10, 39, 144, 14);
			contentPanel.add(lblNameAppo);
			
			textField_Name = new JTextField();
			textField_Name.setBounds(164, 36, 114, 20);
			contentPanel.add(textField_Name);
			textField_Name.setColumns(10);
			
			textField_Location = new JTextField();
			textField_Location.setBounds(164, 61, 114, 20);
			contentPanel.add(textField_Location);
			textField_Location.setColumns(10);
			
			JLabel lblOrt = new JLabel("Ort ");
			lblOrt.setBounds(10, 64, 144, 14);
			contentPanel.add(lblOrt);
			
			JLabel lblKalender = new JLabel("Kalender");
			lblKalender.setBounds(10, 89, 144, 14);
			contentPanel.add(lblKalender);
			
			cbCalendars = new JComboBox();
			cbCalendars.setBounds(164, 86, 42, 20);
			contentPanel.add(cbCalendars);
			
			JButton btnCreate = new JButton("Best\u00E4tigen");
			btnCreate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(rdbtnKalender.isSelected()){
						gui.addNewCalendar();
					}
					else if(rdbtnTermin.isSelected()){
						gui.addNewEntry();
					}
					else if(rdbtnAufgabe.isSelected()){
						gui.addNewEntry();
					}
				}
			});
			btnCreate.setBounds(172, 345, 144, 23);
			contentPanel.add(btnCreate);
		}
		
		rdbtnTermin = new JRadioButton("Termin");
		rdbtnTermin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnKalender.setSelected(false);
				rdbtnAufgabe.setSelected(false);
				
				textField_YearFrom.setEditable(true);
				textField_MonthFrom.setEditable(true);
				textField_DayFrom.setEditable(true);
				textField_MinuteFrom.setEditable(true);
				textField_HourFrom.setEditable(true);
				textField_YearTo.setEditable(true);
				textField_MonthTo.setEditable(true);
				textField_DayTo.setEditable(true);
				textField_HourTo.setEditable(true);
				textField_MinuteTo.setEditable(true);
				textField_Location.setEditable(true);
				cbCalendars.setEditable(true);
			}
		});
		rdbtnTermin.setBounds(14, 110, 109, 23);
		contentPanel.add(rdbtnTermin);
		
		rdbtnAufgabe = new JRadioButton("Aufgabe");
		rdbtnAufgabe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnKalender.setSelected(false);
				rdbtnTermin.setSelected(false);
				
				textField_YearFrom.setEditable(true);
				textField_MonthFrom.setEditable(true);
				textField_DayFrom.setEditable(true);
				textField_MinuteFrom.setEditable(true);
				textField_HourFrom.setEditable(true);
				textField_YearTo.setEditable(true);
				textField_MonthTo.setEditable(true);
				textField_DayTo.setEditable(true);
				textField_HourTo.setEditable(true);
				textField_MinuteTo.setEditable(true);
				textField_Location.setEditable(true);
				cbCalendars.setEditable(true);
			}
		});
		rdbtnAufgabe.setBounds(124, 110, 109, 23);
		contentPanel.add(rdbtnAufgabe);
		
		rdbtnKalender = new JRadioButton("Kalender");
		rdbtnKalender.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnTermin.setSelected(false);
				rdbtnAufgabe.setSelected(false);
				
				textField_YearFrom.setEditable(false);
				textField_MonthFrom.setEditable(false);
				textField_DayFrom.setEditable(false);
				textField_MinuteFrom.setEditable(false);
				textField_HourFrom.setEditable(false);
				textField_YearTo.setEditable(false);
				textField_MonthTo.setEditable(false);
				textField_DayTo.setEditable(false);
				textField_HourTo.setEditable(false);
				textField_MinuteTo.setEditable(false);
				textField_Location.setEditable(false);
				cbCalendars.setEditable(false);
			}
		});
		rdbtnKalender.setBounds(235, 110, 109, 23);
		contentPanel.add(rdbtnKalender);
		
		textField_YearFrom = new JTextField();
		textField_YearFrom.setBounds(72, 140, 62, 20);
		contentPanel.add(textField_YearFrom);
		textField_YearFrom.setColumns(10);
		
		textField_MonthFrom = new JTextField();
		textField_MonthFrom.setColumns(10);
		textField_MonthFrom.setBounds(144, 140, 62, 20);
		contentPanel.add(textField_MonthFrom);
		
		textField_DayFrom = new JTextField();
		textField_DayFrom.setColumns(10);
		textField_DayFrom.setBounds(216, 140, 62, 20);
		contentPanel.add(textField_DayFrom);
		
		textField_HourFrom = new JTextField();
		textField_HourFrom.setColumns(10);
		textField_HourFrom.setBounds(288, 140, 62, 20);
		contentPanel.add(textField_HourFrom);
		
		textField_MinuteFrom = new JTextField();
		textField_MinuteFrom.setColumns(10);
		textField_MinuteFrom.setBounds(360, 140, 62, 20);
		contentPanel.add(textField_MinuteFrom);
		
		textField_YearTo = new JTextField();
		textField_YearTo.setColumns(10);
		textField_YearTo.setBounds(72, 185, 62, 20);
		contentPanel.add(textField_YearTo);
		
		textField_MonthTo = new JTextField();
		textField_MonthTo.setColumns(10);
		textField_MonthTo.setBounds(144, 185, 62, 20);
		contentPanel.add(textField_MonthTo);
		
		textField_DayTo = new JTextField();
		textField_DayTo.setColumns(10);
		textField_DayTo.setBounds(216, 185, 62, 20);
		contentPanel.add(textField_DayTo);
		
		textField_HourTo = new JTextField();
		textField_HourTo.setColumns(10);
		textField_HourTo.setBounds(288, 185, 62, 20);
		contentPanel.add(textField_HourTo);
		
		textField_MinuteTo = new JTextField();
		textField_MinuteTo.setColumns(10);
		textField_MinuteTo.setBounds(360, 185, 62, 20);
		contentPanel.add(textField_MinuteTo);
		
		JLabel lblJahr = new JLabel("Jahr");
		lblJahr.setHorizontalAlignment(SwingConstants.CENTER);
		lblJahr.setBounds(72, 160, 62, 14);
		contentPanel.add(lblJahr);
		
		JLabel lblMonat = new JLabel("Monat");
		lblMonat.setHorizontalAlignment(SwingConstants.CENTER);
		lblMonat.setBounds(144, 160, 62, 14);
		contentPanel.add(lblMonat);
		
		JLabel lblTag = new JLabel("Tag");
		lblTag.setHorizontalAlignment(SwingConstants.CENTER);
		lblTag.setBounds(216, 160, 62, 14);
		contentPanel.add(lblTag);
		
		JLabel lblStunde = new JLabel("Stunde");
		lblStunde.setHorizontalAlignment(SwingConstants.CENTER);
		lblStunde.setBounds(288, 160, 62, 14);
		contentPanel.add(lblStunde);
		
		JLabel lblMinute = new JLabel("Minute");
		lblMinute.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinute.setBounds(360, 160, 62, 14);
		contentPanel.add(lblMinute);
		
		JLabel label = new JLabel("Jahr");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(72, 203, 62, 14);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("Monat");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(144, 203, 62, 14);
		contentPanel.add(label_1);
		
		JLabel label_2 = new JLabel("Tag");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setBounds(216, 203, 62, 14);
		contentPanel.add(label_2);
		
		JLabel label_3 = new JLabel("Stunde");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setBounds(288, 203, 62, 14);
		contentPanel.add(label_3);
		
		JLabel label_4 = new JLabel("Minute");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setBounds(360, 203, 62, 14);
		contentPanel.add(label_4);
		
		JLabel lblNewLabel = new JLabel("Von:");
		lblNewLabel.setBounds(10, 143, 46, 14);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Bis");
		lblNewLabel_1.setBounds(10, 188, 46, 14);
		contentPanel.add(lblNewLabel_1);
		
		textField_Description = new JTextField();
		textField_Description.setBounds(10, 244, 407, 90);
		contentPanel.add(textField_Description);
		textField_Description.setColumns(10);
		
		JLabel lblBeschreibung = new JLabel("Beschreibung");
		lblBeschreibung.setBounds(10, 223, 93, 14);
		contentPanel.add(lblBeschreibung);
		
		JButton btnexportCalendar = new JButton("Kalender exportieren");
		btnexportCalendar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String calendarName = (String) cbCalendars.getSelectedItem();
				gui.exportCalendar(calendarName);
				//gui.showError("Fehler: Diese Funktion ist noch nicht \n verf�gbar. Unvollst�ndige Datenklasse.");
			}
		});
		// �ffnet fileCooser und f�hrt die import methode in der Gui aus
		JButton btnImportCalendar = new JButton("Kalender importieren");
		btnImportCalendar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jfc = new JFileChooser();
				jfc.showOpenDialog(null);
				File f = jfc.getSelectedFile();
				gui.importCalendar(f);
			}
		});
		
		btnImportCalendar.setBounds(10, 400, 145, 23);
		contentPanel.add(btnImportCalendar);
		
		btnexportCalendar.setBounds(172, 400, 144, 23);
		contentPanel.add(btnexportCalendar);
		
		jtModelC = new DefaultTableModel(namenCal, 0);
		
		jtModelA = new DefaultTableModel(namenAppo, 0);

		JCheckBox chckbxNewCheckBox = new JCheckBox("Favorit");
		chckbxNewCheckBox.setBounds(14, 345, 140, 23);
		contentPanel.add(chckbxNewCheckBox);	
	}
	
	public int getYFrom(){
		int ret = Integer.parseInt(textField_YearFrom.getText());
		return ret;
	}
	public int getMFrom(){
		int ret = Integer.parseInt(textField_MonthFrom.getText());
		return ret;
	}
	
	public int getDFrom(){
		int ret = Integer.parseInt(textField_DayFrom.getText());
		return ret;
	}
	
	public int getHFrom(){
		int ret = Integer.parseInt(textField_HourFrom.getText());
		return ret;
	}
	
	public int getMinFrom(){
		int ret = Integer.parseInt(textField_MinuteFrom.getText());
		return ret;
	}
	
	public int getYTo(){
		int ret = Integer.parseInt(textField_YearTo.getText());
		return ret;
	}
	public int getMTo(){
		int ret = Integer.parseInt(textField_MonthTo.getText());
		return ret;
	}
	
	public int getDTo(){
		int ret = Integer.parseInt(textField_DayTo.getText());
		return ret;
	}
	
	public int getHTo(){
		int ret = Integer.parseInt(textField_HourTo.getText());
		return ret;
	}
	
	public int getMinTo(){
		int ret = Integer.parseInt(textField_MinuteTo.getText());
		return ret;
	}
	
	public String getDescription(){
		return textField_Description.getText();
	}
	
	public String getName(){
		return textField_Name.getText();
	}
	
	public String getLocation0(){
		return textField_Location.getText();
	}
	
	public String getSelectedType(){
		String type = null;
		if(rdbtnKalender.isSelected()){
			type = "Calendar";
		}
		else if(rdbtnTermin.isSelected()){
			type = "Appointment";
		}
		else if(rdbtnAufgabe.isSelected()){
			type = "Task";
		}
		return type;
	}
	//Write the calendar names into the combobox
	public void setCalsInCB(Calendar[] pCals){
		cbCalendars.removeAllItems();
		for(int i=0;i<pCals.length;i++){
			cbCalendars.addItem(pCals[i].getName());
		}
	}
	//get calendar name which is selected in the combobox
	public String getSelectedCalCBItem(){
		String item = (String)cbCalendars.getSelectedItem();
		return item;
	}
}
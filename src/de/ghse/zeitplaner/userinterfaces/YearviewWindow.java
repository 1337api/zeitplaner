package de.ghse.zeitplaner.userinterfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.ghse.zeitplaner.Gui;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class YearviewWindow extends JDialog {
	
	private Gui gui;
	private final JPanel contentPanel = new JPanel();
	String[] columnsY = {"Januar","Februar","M�rz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"};
	String[][] rowsY = {{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},{"", "", "","", "","", "","", "","", "",""},};
	private JTable jtYearView;
	/**
	 * Launch the application.
	 */
	public void initialize() {
		try {
			
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public YearviewWindow(Gui pGui) {
		setTitle("Jahresansicht");
		gui = pGui;
		setBounds(100, 100, 802, 421);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		jtYearView = new JTable(rowsY, columnsY);
		jtYearView.setBounds(50, 50, 200, 200);
		
		JScrollPane scrollPane = new JScrollPane(jtYearView);
		scrollPane.setBounds(10, 11, 766, 327);
		contentPanel.add(scrollPane);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						gui.closeYearWindow();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}

package de.ghse.zeitplaner.resources;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ICalAppointment extends Entry {
	
	private String transp;
	private String url;
	private static int AppoID = 0;
	private String calName;

	public ICalAppointment(LocalDateTime pTimeStart, LocalDateTime pTimeFinish){
		super(pTimeStart, pTimeFinish);
	}
	
	public static ICalAppointment of(Entry pEntry){	// Creates an ICalAppointment out of an Entry
		ICalAppointment newAppo = new ICalAppointment(pEntry.getDtstart(), pEntry.getDtend());
		
		if(pEntry.getcClass()!=null)
		newAppo.setClass(pEntry.getcClass());
		
		if(pEntry.getDtstart()!=null)
			newAppo.setDtstart(pEntry.getDtstart());
		
		if(pEntry.getDtend()!=null)
			newAppo.setDtend(pEntry.getDtend());
		
		newAppo.setUid(newAppo.generateUID());

		if(pEntry.getCreated()!=null)
		newAppo.setCreated(pEntry.getCreated());
		
		if(pEntry.getDescription()!=null)
		newAppo.setDescription(pEntry.getDescription());
		
		if(pEntry.getGeo()!=null)
		newAppo.setGeo(pEntry.getGeo());
		
		if(pEntry.getLocation() !=null)
		newAppo.setLocation(pEntry.getLocation());
		
		if(pEntry.getOrganizer()!=null)
		newAppo.setOrganizer(pEntry.getOrganizer());
		
		if(pEntry.getPriority()!=null)
		newAppo.setPriority(pEntry.getPriority());
		
		if(pEntry.getRecurid()!=null)
		newAppo.setRecurid(pEntry.getRecurid());
		
		if(pEntry.getStatus()!=null)
		newAppo.setStatus(pEntry.getStatus());
		
		if(pEntry.getSummary()!=null)
		newAppo.setSummary(pEntry.getSummary());
		
		if(pEntry.getDuration()!=null)
		newAppo.setDuration(pEntry.getDuration());
		
		return newAppo;
	}
	
	public void init() {
		uid = generateUID();
	}
		
	public String generateUID() {
		String UID = ldt2ictString(dtstart, false)
				+ "-" 
				+ ldt2ictString(dtend, false) 
				+ "-" 
				+ AppoID
				+ "@planer.tgi12.ghse.de";
		AppoID++;

		return UID;
	}

	
//Get- & Set- Methods	
	//TODO
	
	public String getTransp() {
		return transp;
	}

	public void setTransp(String transp) {
		this.transp = transp;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setCalName(String name){
		calName = name;
	}
	
	public String getCalName(){
		return calName;
	}
	
	public String[] generateICS(){
		String[] icsArray = new String[100];
		icsArray[0] = "BEGIN:VEVENT" + "\r";
		
		if(getcClass()!=null)
			icsArray[1] = "CLASS:" +getcClass(); 
		if(getCreated()!=null)
			icsArray[2] = "CREATED:" +getCreated(); 
		if(getDescription()!=null)
			icsArray[3] = "DESCRIPTION:" +getDescription(); 
		if(getDtstart()!=null)
			icsArray[4] = "DTSTART:" +ldt2ictString(getDtstart(), false); 
		if(getDtend()!=null)
			icsArray[5] = "DTEND:" +ldt2ictString(getDtend(), false);
		if(getDtstamp()!=null)
			icsArray[6] = "DTSTAMP:" +ldt2ictString(getDtstamp(), true);  
		if(getGeo()!=null)
			icsArray[7] = "GEO:" +getGeo(); 
		if(getOrganizer()!=null)
			icsArray[8] = "ORGANIZER:" +getOrganizer(); 
		if(getPriority()!=null)
			icsArray[9] = "PRIORITY:" +getPriority(); 
		if(getSeq()!=null)
			icsArray[10] = "SEQ:" +getSeq(); 
		if(getStatus()!=null)
			icsArray[11] = "STATUS:" +getStatus(); 
		if(getSummary()!= null)
			icsArray[12] = "SUMMARY:" +getSummary();
		if(getTransp()!= null)
			icsArray[13] = "TRANSP:" +getTransp();
		if(getUid()!= null)
			icsArray[14] = "UID:" +getUid();
		if(getUrl()!= null)
			icsArray[15] = "URL:" +getUrl();
		if(getRecurid()!= null)
			icsArray[16] = "RECURID:" +getRecurid();
		
		icsArray[17] = "END:VEVENT";
		
		return icsArray;	
	}

}

package de.ghse.zeitplaner.resources;

public class StandardTime extends TimezoneProp {
	  
	  // Anfang Attribute
	  // Ende Attribute
	  
	  public StandardTime() {
	  }
	  
	  public static StandardTime of(TimezoneProp tzProp){
		  StandardTime standard = new StandardTime();
		  
		  if(tzProp.getDtstart()!=null)
			  standard.setDtstart(tzProp.getDtstart());
				
		  if(tzProp.getTZName()!=null)
			  standard.setTZName(tzProp.getTZName());
		  
		  if(tzProp.getTzoffsetto()!=null)
			  standard.setTzoffsetto(tzProp.getTzoffsetto());
		  
		  if(tzProp.getZtoffsetfrom()!=null)
			  standard.setZtoffsetfrom(tzProp.getZtoffsetfrom());
		  
		return standard;
	  }

	  // Anfang Methoden
	  // Ende Methoden
	} // end of standardc

package de.ghse.zeitplaner.resources;

import java.time.LocalDateTime;

public class ICalTask extends Entry {

	// Begin attributes
	private String completed;
	private LocalDateTime due;
	private String calName;
	// End attributes

	// Begin constructor
	public ICalTask(){
		setSummary("Neue Aufgabe");
		setDtstamp(LocalDateTime.now());
	}
	
	public ICalTask(LocalDateTime pDtStart, LocalDateTime pDtEnd){
		this();
		this.setDtstart(pDtStart);
		this.setDtend(pDtEnd);
	}
	
	public ICalTask(LocalDateTime pDtStart, String pDuration){
		this();
		this.setDtstart(pDtStart);
		this.setDuration(pDuration);
	}
	// End constructor

	// Begin methods
	public static ICalTask of(Entry pEntry) {		// Creates an ICalTask out of an Entry
		ICalTask newTask = new ICalTask(pEntry.getDtstart(), pEntry.getDtend());

		if (pEntry.getcClass() != null)
			newTask.setClass(pEntry.getcClass());

		newTask.setUid(newTask.generateUID());

		if (pEntry.getCreated() != null)
			newTask.setCreated(pEntry.getCreated());

		if (pEntry.getDescription() != null)
			newTask.setDescription(pEntry.getDescription());

		if (pEntry.getGeo() != null)
			newTask.setGeo(pEntry.getGeo());

		if (pEntry.getLocation() != null)
			newTask.setLocation(pEntry.getLocation());

		if (pEntry.getOrganizer() != null)
			newTask.setOrganizer(pEntry.getOrganizer());

		if (pEntry.getPriority() != null)
			newTask.setPriority(pEntry.getPriority());

		if (pEntry.getRecurid() != null)
			newTask.setRecurid(pEntry.getRecurid());

		if (pEntry.getStatus() != null)
			newTask.setStatus(pEntry.getStatus());

		if (pEntry.getSummary() != null)
			newTask.setSummary(pEntry.getSummary());

		if (pEntry.getDuration() != null)
			newTask.setDuration(pEntry.getDuration());

		return newTask;
	}
	public String generateUID() {	// �berlege dir wie du die UID einer Aufgabe zusammensetzt. Sie darf nicht identisch mit irgendeiner ID eines Appointments sein! Sollte aber �hnlich strukturiert sein.
		return null;
	}

	public String getCompleted() {
		return completed;
	}

	public void setCompleted(String completed) {
		this.completed = completed;
	}

	public LocalDateTime getDue() {
		return due;
	}

	public void setDue(LocalDateTime due) {
		this.due = due;
	}
	
	public void setCalName(String name){
		calName = name;
	}
	
	public String getCalName(){
		return calName;
	}
	
	public String[] generateICS(){
		String[] icsArray = new String[100];
		icsArray[0] = "BEGIN:VEVENT" + "\r";
		
		if(getcClass()!=null)
			icsArray[1] = "CLASS:"+getcClass(); 
		if(getCreated()!=null)
			icsArray[2] = "CREATED:" + getCreated(); 
		if(getDescription()!=null)
			icsArray[3] = "DESCRIPTION:" +getDescription(); 
		if(getDtstart()!=null)
			icsArray[4] = "DTSTART:" +ldt2ictString(getDtstart(), false); 
		if(getDtend()!=null)
			icsArray[5] = "DTEND:" +ldt2ictString(getDtend(), false);
		if(getDtstamp()!=null)
			icsArray[6] = "DTSTAMP:" +ldt2ictString(getDtstamp(), true);  
		if(getGeo()!=null)
			icsArray[7] = "CREATED:" +getGeo(); 
		if(getOrganizer()!=null)
			icsArray[8] = "ORGANIZER:" +getOrganizer(); 
		if(getPriority()!=null)
			icsArray[9] = "PRIORITY:" +getPriority(); 
		if(getSeq()!=null)
			icsArray[10] = "SEQ:" +getSeq(); 
		if(getStatus()!=null)
			icsArray[11] = "STATUS:" +getStatus(); 
		if(getSummary()!= null)
			icsArray[12] = "SUMMARY:" +getSummary();
		if(getCompleted()!= null)
			icsArray[13] = "COMPLETED:" +getCompleted();
		if(getUid()!= null)
			icsArray[14] = "UID:" +getUid();
		if(getDue()!= null)
			icsArray[15] = "DUE:" +ldt2ictString(getDue(), false);
		if(getRecurid()!= null)
			icsArray[16] = "RECURID:" +getRecurid();
		
		icsArray[17] = "END:VTODO";
		
		return icsArray;	
	}
	// End methods

} // end of ToDo

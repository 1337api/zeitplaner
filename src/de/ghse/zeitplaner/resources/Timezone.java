package de.ghse.zeitplaner.resources;

public class Timezone extends CalendarComponent {
	  
	  // Anfang Attribute
	  private StandardTime standardc;
	  private Daylight daylightc;
	  private String timeZoneID;
	  // Ende Attribute
	  
	  public Timezone() {
	    this.standardc = new StandardTime();;
	    this.daylightc = new Daylight();
	  }

	  // Anfang Methoden
	  public Daylight getDaylightc() {
	    return daylightc;
	  }

	  public void setStandard(StandardTime pStandard){
		  standardc = pStandard;
	  }
	  
	  public void setDaylight(Daylight pDaylight){
		  daylightc = pDaylight;
	  }
	  
	  public void setTimeZoneID(String pTZID){
		  timeZoneID = pTZID;
	  }
	  
	  public String getTimeZoneID(){
		  return timeZoneID;
	  }
	  // Ende Methoden
	} // end of Timezone

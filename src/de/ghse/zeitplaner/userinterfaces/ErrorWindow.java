package de.ghse.zeitplaner.userinterfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.ghse.zeitplaner.Gui;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class ErrorWindow extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblErrorMessage;
	private Gui gui;

	/**
	 * Launch the application.
	 */
	public  void initialize() {
		try {
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ErrorWindow(Gui pGui) {
		gui = pGui;
		this.setTitle("Error");
		setBounds(100, 100, 667, 444);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		ImageIcon errorImage = new ImageIcon("error.png");
		{
			JLabel lblNewLabel = new JLabel(errorImage);
			lblNewLabel.setBounds(167, 11, 300, 272);
			contentPanel.add(lblNewLabel);
		}
		
		lblErrorMessage = new JLabel("Fehler");
		lblErrorMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorMessage.setBounds(10, 300, 631, 61);
		contentPanel.add(lblErrorMessage);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						gui.closeErrorWindow();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						gui.closeErrorWindow();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	public void printError(String pError){
		lblErrorMessage.setText(pError);
		this.setVisible(true);
	}
}

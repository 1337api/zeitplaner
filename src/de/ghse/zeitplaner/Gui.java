package de.ghse.zeitplaner;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import de.ghse.zeitplaner.resources.Calendar;
import de.ghse.zeitplaner.resources.ICalAppointment;
import de.ghse.zeitplaner.resources.ICalTask;
import de.ghse.zeitplaner.userinterfaces.AppointmentWindow;
import de.ghse.zeitplaner.userinterfaces.DayviewWindow;
import de.ghse.zeitplaner.userinterfaces.ErrorWindow;
import de.ghse.zeitplaner.userinterfaces.MonthviewWindow;
import de.ghse.zeitplaner.userinterfaces.OptionWindow;
import de.ghse.zeitplaner.userinterfaces.WeekviewWindow;
import de.ghse.zeitplaner.userinterfaces.YearviewWindow;

import java.awt.FlowLayout;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.time.LocalDateTime;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Component;
import javax.swing.JLabel;
import java.awt.Toolkit;

public class Gui {
	private Control ctrl = new Control(this);
	private JFrame frmOpKalender;
	private AppointmentWindow aw = new AppointmentWindow(this);
	private DayviewWindow dvw = new DayviewWindow(this);
	private WeekviewWindow wvw = new WeekviewWindow(this);
	private MonthviewWindow mvw = new MonthviewWindow(this);
	private YearviewWindow jvw = new YearviewWindow(this);
	private ErrorWindow ew = new ErrorWindow(this);
	private OptionWindow ow = new OptionWindow(this);
	private TableModel jtModelC;
	private TableModel jtModelA;
	private TableModel jtModelT;
	JScrollPane scrollPaneTask;
	JScrollPane scrollPaneAppointment;
	JScrollPane scrollPaneCalendar;
	private JTable jtCalendar;
	private JTable jtAppointment;
	private JTable jtTask;
	private int kontostand = 0;
	int rowCountC;
	int columnCountC;
	int rowCountA;
	int columnCountA;
	private String  sName = "Name";
	private String  sBelongingCal = "Zugeh�riger Kalender";
	private String  sFrom = "Von";
	private String  sTo = "Bis";
	private String  sLoc = "Ort";
	private String  sDuration = "Dauer";
	
	String[] Calendars = { "Kalenderansichten", "Tagesansicht", "Wochenansicht", "Monatsansicht", "Jahresansicht" };
	private String[] namenCal = { sName };
	private String[] namenAppo = { sBelongingCal, sName, sFrom, sTo, sLoc };
	private String[] namenTask = { sBelongingCal, sName, sFrom, sTo, sDuration, sLoc };
	private JButton btnOptionen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frmOpKalender.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmOpKalender = new JFrame();
		frmOpKalender.setTitle("OP - Kalender");
		frmOpKalender.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\Corvin\\Desktop\\Wallpaper\\behinderung-symbol_318-27585.jpg"));
		frmOpKalender.setBounds(0, 0, 1920, 1021);
		frmOpKalender.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton btnShowAppoView = new JButton("Erstellen");
		btnShowAppoView.setBounds(33, 855, 140, 39);
		btnShowAppoView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				aw.initialize();
			}
		});
		frmOpKalender.getContentPane().setLayout(null);
		frmOpKalender.getContentPane().add(btnShowAppoView);

		JButton btnDonate = new JButton("Donate");
		btnDonate.setBounds(33, 948, 98, 23);
		btnDonate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				kontostand = kontostand + 100;
				ew.printError("Achtung, wir haben so eben " + kontostand + "� von ihrem Konto abgezogen. Wir bitten um Verst�ndnis.");
			}
		});
		frmOpKalender.getContentPane().add(btnDonate);

		JComboBox<String> comboBoxCalViews = new JComboBox<String>();
		comboBoxCalViews.setBounds(33, 6, 140, 39);
		comboBoxCalViews.setModel(new DefaultComboBoxModel<String>(Calendars));

		comboBoxCalViews.setMaximumRowCount(4);
		frmOpKalender.getContentPane().add(comboBoxCalViews);

		comboBoxCalViews.setSelectedIndex(0);

		jtModelC = new DefaultTableModel(namenCal, 0);
		jtCalendar = new JTable(jtModelC);
		jtCalendar.setBounds(50, 50, 200, 200);

		scrollPaneCalendar = new JScrollPane(jtCalendar);
		scrollPaneCalendar.setBounds(33, 77, 1800, 220);
		frmOpKalender.getContentPane().add(scrollPaneCalendar);

		jtModelA = new DefaultTableModel(namenAppo, 0);
		jtAppointment = new JTable(jtModelA);
		jtAppointment.setBounds(50, 50, 200, 200);

		scrollPaneAppointment = new JScrollPane(jtAppointment);
		scrollPaneAppointment.setBounds(33, 337, 1800, 220);
		frmOpKalender.getContentPane().add(scrollPaneAppointment);

		jtModelT = new DefaultTableModel(namenTask, 0);
		jtTask = new JTable(jtModelT);
		jtTask.setBounds(50, 50, 200, 200);

		scrollPaneTask = new JScrollPane(jtTask);
		scrollPaneTask.setBounds(33, 597, 1800, 220);
		frmOpKalender.getContentPane().add(scrollPaneTask);

		JLabel lblNewLabel = new JLabel("Kalender:");
		lblNewLabel.setBounds(33, 54, 73, 14);
		frmOpKalender.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Termine:");
		lblNewLabel_1.setBounds(33, 312, 56, 14);
		frmOpKalender.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Aufgaben:");
		lblNewLabel_2.setBounds(33, 572, 91, 14);
		frmOpKalender.getContentPane().add(lblNewLabel_2);

		btnOptionen = new JButton("Optionen");
		btnOptionen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ow.initialize();
			}
		});
		btnOptionen.setBounds(1697, 6, 136, 39);
		frmOpKalender.getContentPane().add(btnOptionen);

		JButton btnNewButton = new JButton("Bearbeiten");
		btnNewButton.setBounds(183, 855, 140, 39);
		frmOpKalender.getContentPane().add(btnNewButton);
		
		LocalDateTime now = LocalDateTime.now();
		JLabel lblToday = new JLabel(""+now.getDayOfWeek()+", " + now.getDayOfMonth() + " " + now.getMonth()+ " "+ now.getYear());
		lblToday.setBounds(236, 18, 178, 27);
		frmOpKalender.getContentPane().add(lblToday);

		comboBoxCalViews.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (comboBoxCalViews.getSelectedIndex() == 1) {
					dvw.initialize();
				} else if (comboBoxCalViews.getSelectedIndex() == 2) {
					wvw.initialize();
				} else if (comboBoxCalViews.getSelectedIndex() == 3) {
					mvw.initialize();
				} else if (comboBoxCalViews.getSelectedIndex() == 4) {
					jvw.initialize();
				}
			}
		});

	}

	public ErrorWindow getEW() {
		return ew;
	}

	public OptionWindow getOW() {
		return ow;
	}

	public AppointmentWindow getAW() {
		return aw;
	}

	public YearviewWindow getJVW() {
		return jvw;
	}

	public MonthviewWindow getMVW() {
		return mvw;
	}

	public WeekviewWindow getWVW() {
		return wvw;
	}

	public DayviewWindow getDVW() {
		return dvw;
	}

	public void importCalendar(File pFile) {
		ctrl.importICalFile(pFile.getAbsolutePath());
	}

	public void exportCalendar(String pCalendarName) {
		ctrl.exportICalFile(pCalendarName);
	}

	public int getYFrom() {
		int ret = (aw.getYFrom());
		return ret;
	}

	public int getMFrom() {
		int ret = (aw.getMFrom());
		return ret;
	}

	public int getDFrom() {
		int ret = (aw.getDFrom());
		return ret;
	}

	public int getHFrom() {
		int ret = (aw.getHFrom());
		return ret;
	}

	public int getMinFrom() {
		int ret = (aw.getMinFrom());
		return ret;
	}

	public int getYTo() {
		int ret = (aw.getYTo());
		return ret;
	}

	public int getMTo() {
		int ret = (aw.getMTo());
		return ret;
	}

	public int getDTo() {
		int ret = (aw.getDTo());
		return ret;
	}

	public int getHTo() {
		int ret = (aw.getHTo());
		return ret;
	}

	public int getMinTo() {
		int ret = (aw.getMinTo());
		return ret;
	}

	public String getDescription() {
		return aw.getDescription();
	}

	public String getName() {
		return aw.getName();
	}

	public String getLocation0() {
		return aw.getLocation0();
	}

	public void printCalendars(Calendar[] cals) {
		setCalendars(cals);
	}

	public void addNewCalendar() {
		ctrl.createAndAddICalendar();
	}

	public void closeAppoWindow() {
		aw.setVisible(false);
	}

	public void closeErrorWindow() {
		ew.setVisible(false);
	}

	public void closeYearWindow() {
		jvw.setVisible(false);
	}

	public void closeWeekWindow() {
		wvw.setVisible(false);
	}

	public void closeDayWindow() {
		dvw.setVisible(false);
	}

	public void closeMonthWindow() {
		mvw.setVisible(false);
	}

	public void closeOptionWindow() {
		ow.setVisible(false);
	}

	public void showError(String pError) {
		ew.printError(pError);
	}
	
	public void setCalendars(Calendar[] cals) {
		//Delete everything from Calendars 
		jtModelA = null;		
		jtAppointment = null;
		scrollPaneAppointment = null;
		
		jtModelC = null;
		jtCalendar = null;
		scrollPaneCalendar = null;
		
		jtModelT = null;
		jtTask = null;
		scrollPaneTask = null;
		
		int appoHeight = 0;
		int taskHeight = 0;
		int calHeight = 0;
		
		// Check number of cals, tasks and appos
		for(int i=0;i<cals.length;i++){
			if(cals[i]!=null){
				appoHeight = appoHeight + cals[i].getIndexEvents();
				taskHeight = taskHeight + cals[i].getIndexTask();
				calHeight = cals.length;
			}
		}
		
		// create new jtModelC
		jtModelC = new DefaultTableModel(namenCal, calHeight);
		jtCalendar = new JTable(jtModelC);
		jtCalendar.setBounds(50, 50, 200, 200);
		
		//Print Calendars
		for(int i=0;i<calHeight;i++){
			jtModelC.setValueAt(cals[i].getName(), i, 0);
		}
		
		scrollPaneCalendar = new JScrollPane(jtCalendar);
		scrollPaneCalendar.setBounds(33, 77, 1800, 220);
		frmOpKalender.getContentPane().add(scrollPaneCalendar);
		
		
		
		// create new jtModelA
		jtModelA = new DefaultTableModel(namenAppo, appoHeight);
		jtAppointment = new JTable(jtModelA);
		jtAppointment.setBounds(50, 100, 200, 200);
		
		// Save all appointments in array
		ICalAppointment[] appos = new ICalAppointment[appoHeight];
		int appoIndex=0;
		for(int i=0;i<cals.length;i++){
			for(int k=0; k<cals[i].getIndexEvents();k++){
				appos[appoIndex] = cals[i].getEvent(k);
				appoIndex++;
			}
		}
		
		// Print appointments
		for(int i=0;i<appoHeight;i++){
			String calName = ctrl.getCalByName(appos[i].getCalName()).getName();
			jtModelA.setValueAt(calName, i, 0);
			jtModelA.setValueAt(appos[i].getSummary(), i, 1);
			jtModelA.setValueAt(appos[i].getDtstart(), i, 2);
			jtModelA.setValueAt(appos[i].getDtend(), i, 3);
			jtModelA.setValueAt(appos[i].getLocation(), i, 4);
		}
		
		// Overwrite old ScrollPane with new JTable 
		scrollPaneAppointment = new JScrollPane(jtAppointment);
		scrollPaneAppointment.setBounds(33, 337, 1800, 220);
		frmOpKalender.getContentPane().add(scrollPaneAppointment);
		
		
		
		
		// Overwrite old Tablemodel and JTable
		jtModelT = new DefaultTableModel(namenTask, taskHeight);
		jtTask = new JTable(jtModelT);
		jtTask.setBounds(50, 50, 200, 200);
		
		
		// Get all tasks which are inside the parameter
		ICalTask[] tasks = new ICalTask[taskHeight];
		int taskIndex=0;
		for(int i=0;i<cals.length;i++){
			for(int k=0; k<cals[i].getIndexTask();k++){
				tasks[taskIndex] = cals[i].getTask(k);
				taskIndex++;
			}
		}
		
		// Print all tasks 
		for(int i=0;i<taskHeight;i++){
			String calName = " ";
			String name = " "; 
			String from = " ";
			String to = " ";
			String due = " ";
			String loc = " "; 
			
			if(ctrl.getCalByName(tasks[i].getCalName()).getName()!=null)
				calName = ctrl.getCalByName(tasks[i].getCalName()).getName();
			
			if(tasks[i].getDtstart()!=null)
				from = tasks[i].getDtstart().toString();
			
			if(tasks[i].getDtend()!=null)
				to = tasks[i].getDtend().toString();
			
			if(tasks[i].getDue()!=null)
				due = tasks[i].getDue().toString();
			
			if(tasks[i].getSummary()!=null)
				name = tasks[i].getSummary();
			
			if(tasks[i].getLocation()!=null)
				loc = tasks[i].getLocation();
			
			jtModelT.setValueAt(calName, i, 0);
			jtModelT.setValueAt(name, i, 1);
			jtModelT.setValueAt(from, i, 2);
			jtModelT.setValueAt(to, i, 3);
			jtModelT.setValueAt(due, i, 4);
			jtModelT.setValueAt(loc, i, 5);
		}
		
		// Overwrite old ScrollPane
		scrollPaneTask = new JScrollPane(jtTask);
		scrollPaneTask.setBounds(33, 597, 1800, 220);
		frmOpKalender.getContentPane().add(scrollPaneTask);
		
		// Send all calendars to the combobox in the AppointmentWindow
		aw.setCalsInCB(cals);
	}

	public String getType() {
		String type = aw.getSelectedType();
		return type;
	}
	
	public void addNewEntry(){
		ctrl.createAndAddEntry();
	}
	
	public String getSelectedCalendarName(){
		return aw.getSelectedCalCBItem();
	}
	
	// doesn�t work -> Should change the language 
	public void setLanguage(String pLanguage){
		if(pLanguage.equals("Deutsch")){
			sName = "Name";
			sBelongingCal = "Zugeh�riger Kalender";
			sFrom = "Von";
			sTo = "Bis";
			sLoc = "Ort";
			sDuration = "Dauer";
		}
		else if(pLanguage.equals("English")){
			sName = "Name";
			sBelongingCal = "Belonging Calendar";
			sFrom = "From";
			sTo = "To";
			sLoc = "Location";
			sDuration = "Duration";
		}
		frmOpKalender.repaint();
		scrollPaneAppointment.repaint();
	}
}
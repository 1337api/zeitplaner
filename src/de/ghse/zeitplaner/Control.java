package de.ghse.zeitplaner;

import de.ghse.zeitplaner.resources.*;
import de.ghse.zeitplaner.sql.DB_Viewer;

//1337api@bitbucket.org/1337api/zeitplaner.git

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Control {

	// --Member variables/Attributes--
	private DB_Viewer dbv = new DB_Viewer("zeitplaner");
	private Resource data = new Resource(dbv);
	private Gui gui;
	private int fileReadingIndex1 = 0;
	private int fileReadingIndex2 = 0;
	private int fileReadingIndex3 = 0;
	private int charIndex;
	private String attribute;
	private String value;
	
	
	String[] fileAsStringArray = new String[1];
	String[] readingArray = new String[2];

	// --Constructor--
	public Control(Gui pGui){
		gui = pGui;
		//initProgram();
	}

	// --Methods--
	public void initProgram() {
		Calendar[] cals = dbv.getCalendars();
		
		// Load calendars from database to local memory
		data.setCals(cals);		

		// Clear all calendars and appointments in GUI and 
		// Print current calendars and appointments in GUI
		gui.printCalendars(cals);
	}
	
	public void updateCalendars(){
		// Get all calendars from the local data structure
		Calendar[] cals = data.getCals();
		
		// Wipe the old calendars and Print the new calendars inside the Graphical User Interface
		gui.printCalendars(cals);
	}
	
	public Calendar createAndAddICalendar() {		//Creates and adds a calendar without parameters -> Standard values
		Calendar iCal = new Calendar(gui.getName());
		iCal.setCalscale("PUBLISH");
		iCal.setMethod("GREGORIAN");

		data.addCal(iCal);
		dbv.execSQL("INSERT INTO Calendars (calscale, method) VALUES ('PUBLISH', 'GREGORIAN');");
		updateCalendars();
		
		return iCal;
	}
	
	public void deleteICalendar(Calendar pCal) { // Method is never used
		Calendar iCal = pCal;	
		data.delCal(iCal);	
		updateCalendars();
	}
	
	public void createAndAddEntry() {
		// Create time points 'From' and 'To' with variables from GUI. Create the timestamp
		LocalDateTime ldtFrom = LocalDateTime.of(gui.getYFrom(), gui.getMFrom(), gui.getDFrom(), gui.getHFrom(),gui.getMinFrom());
		LocalDateTime ldtTo = LocalDateTime.of(gui.getYTo(), gui.getMTo(), gui.getDTo(), gui.getHTo(), gui.getMinTo());

		// Create the entry
		Entry entry = new Entry(ldtFrom, ldtTo);
		entry.setLocation(gui.getLocation0());
		entry.setDescription(gui.getDescription());
		entry.setSummary(gui.getName());

		// Find the right Calendar
		Calendar cal = data.getCalByName(gui.getSelectedCalendarName());
		
		// Check the type
		if(gui.getType().equals("Appointment")){
			ICalAppointment appo = ICalAppointment.of(entry);
			
			// Add appointment to array
			data.addAppointment(cal, appo);
			
			// Add appointment to database
			//dbv.addAppointment(cal.getID(), iCalAppo);
		}
		else if(gui.getType().equals("Task")){
			ICalTask task = ICalTask.of(entry);
			
			// Add appointment to array
			data.addTask(cal, task);
						
			// Add appointment to database
			//dbv.addAppointment(cal.getID(), task);
		}
		updateCalendars();
	}	
	
	public void deleteEvent(int pIdEvent, int pIdCal) { // Method is never used
		data.delEvent(pIdEvent, pIdCal);	
		updateCalendars();
	}
	
	public void deleteTask(int pIdTask, int pIdCal) { 
		//data.delTask(pIdTask, pIdCal); 	// Doesn't exist in data
		updateCalendars();
	}
	
	public void exportICalFile(String pCalendarName) {
		if(checkCalendarExistence(pCalendarName)){
			Calendar cal = data.getCalByName(pCalendarName);
			
			// Get the VEvent body from parameter
			String[] iCalFile = cal.getICS();

			// Add name of calendar to '.ics' in order to create an ics-File (see
			// try-catch)
			String dataName = cal.getName() + ".ics";
			StringBuilder sBuilder = new StringBuilder();
			sBuilder.append(dataName);

			// Creation and writing of ics-File
			try {
				File newIcsFile = new File(sBuilder.toString());

				// Create filewriter, buffer and write vEvent in ics-File
				
				// if file doesnt exist, then create an instance
				if (!newIcsFile.exists()) {
					newIcsFile.createNewFile();

					FileWriter fw = new FileWriter(newIcsFile);
					BufferedWriter bw = new BufferedWriter(fw);
					for (int i = 0; i < iCalFile.length; i++) {
						if (iCalFile[i] != null) {
							bw.write(iCalFile[i] + "\r");
						}
					}
					bw.close();
				}
				

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	public void importICalFile(String pPath) { // Still working on (look at comment->createAndAddICalendar)
		// Convert file to path
		File file = new File(pPath);
		int index;
		String fileName = file.getName();
		index = fileName.indexOf(".");
		String name = fileName.substring(0,index);	// Seperate ".ics" from filename
		
		// Fill the string array with the values from the ICS-data
		file2StringArray(pPath);	
		
		// Include the file in the local data class and database
		convertICalFile2Calendar(name);
	}
	
	private void file2StringArray(String pPath) {	// Read the file and write in in the fileAsStringArray
		fileAsStringArray = new String[1];

		try {
			FileReader fReader = new FileReader(pPath);
			BufferedReader bReader = new BufferedReader(fReader);
			String readLine = "";

			do {
				readLine = bReader.readLine();

				if (readLine != null) {
					fileAsStringArray[fileReadingIndex1] = readLine;

					if (!readLine.equals("END:VCALENDAR")) {
						fileAsStringArray = extendArray(fileAsStringArray);
						fileReadingIndex1++;
					}
				}

			} while (readLine != null);
			bReader.close();
			

		} catch (FileNotFoundException e) {
			System.out.println("Couldn't find the ICS-file");
		} catch (IOException e) {
			System.out.println("Unable to read file");
			// TODO Auto-generated catch block
		}
	}

	private void convertICalFile2Calendar(String pName){	 
		// Check if the file begins with the right tag
		if(checkFirstLine(fileAsStringArray[0])){
			// Create a calendar with the filename
			Calendar cal = new Calendar();
			cal.setName(pName);
			
			// Read fileAsStringArray and include read calendars, tasks and appointments into the data class
			for(int i=0;i<fileAsStringArray.length;i++){
				readSingleLine(i);
				
				// If the read line equals BEGIN:VCALENDAR then initialize the calendar and all its attributes (inside initCalendar the program reads along and synchronizes the reading indexes)
				if((attribute.equals("BEGIN")&& value.equals("VCALENDAR"))){
					fileReadingIndex1 = i;
					initCalendar(cal);
					i = fileReadingIndex1-2;
					data.addCal(cal);
					dbv.addCal(cal);
				}
				// If the read line equals BEGIN:VEVENT or BEGIN:VTODO then initialize an Entry and specify it later to an ICalAppointment or ICalTask.
				else if (attribute.equals("BEGIN") && (value.equals("VEVENT")||value.equals("VTODO"))) {
					int tempIndex = i;
					String type = value;
					
					fileReadingIndex1 = i;
					Entry entry = initEntry();
					i = fileReadingIndex1;
					fileReadingIndex1 = tempIndex;
					   
					if(type.equals("VTODO")){
						ICalTask task = ICalTask.of(entry);
						initTask(task);
						data.addTask(cal, task);
						//dbv.addTask(task);
					}
					else if(type.equals("VEVENT")){
						ICalAppointment appo = ICalAppointment.of(entry);
						initAppointment(appo);
						data.addAppointment(cal, appo);
						//dbv.addAppointment(appo);
					}	
					i = fileReadingIndex1-1;
				}
			}
			fileReadingIndex1 = 0;
			fileReadingIndex2 = 0;
			gui.printCalendars(data.getCals());
			cal.print();
		}
	}
	
	// Check if the first line of an ICS-file is valid 
	private boolean checkFirstLine(String pLine){
		boolean lineIsBeginCalendar = false;
		if(pLine.equals("BEGIN:VCALENDAR")){
			lineIsBeginCalendar = true;
		}
		return lineIsBeginCalendar;
	}
	
	// Check if there is a semicolon inside a line of an ICS-File. If yes, then read the content afterwards
	private int checkSemicolons(int pIndex){
		if(attribute.contains(";")){
			String[] secLine = attribute.split(";");
			attribute = secLine[0];
			String tempContentLine = secLine[1];
			int index = tempContentLine.indexOf("=");
			String newContentLine = tempContentLine.substring(0, index) + ":" + tempContentLine.substring(index+1);
			fileAsStringArray[pIndex] = newContentLine;
			pIndex--;		
		}
		return pIndex;
	}
	
	// This method is used very often, reads a single line during the import of an ICS-File
	private void readSingleLine(int pIndex){
		String line = fileAsStringArray[pIndex];
		charIndex = line.indexOf(':');
		attribute = line.substring(0, charIndex);
		value = line.substring(charIndex+1);
	}
	
	private void initCalendar(Calendar pCal){
		fileReadingIndex1++;
		readSingleLine(fileReadingIndex1);
		
		while((!attribute.equals("BEGIN")&&!value.equals("VEVENT"))||(!attribute.equals("BEGIN")&&!value.equals("VTODO"))){
			readSingleLine(fileReadingIndex1);
			fileReadingIndex1 = checkSemicolons(fileReadingIndex1);
			
			switch (attribute) {
			case "BEGIN":
				if (value.equals("VTIMEZONE")) {
					initTimezone(pCal);
					fileReadingIndex1--;
				}
				break;
			case "METHOD":
				pCal.setMethod(value);
				break;
			case "CALSCALE":
				pCal.setCalscale(value);
				break;
			}
			fileReadingIndex1++;
		}
	}
	
	private void initTimezone(Calendar pCal){
		fileReadingIndex2 = fileReadingIndex1 +1;
		
		readSingleLine(fileReadingIndex2);

		Timezone timezone = new Timezone();
		
		while(!(attribute.equals("END")&&value.equals("VTIMEZONE"))){
			readSingleLine(fileReadingIndex2);
			fileReadingIndex2 = checkSemicolons(fileReadingIndex2);
			
			switch (attribute) {
			case "TZID":
				timezone.setTimeZoneID(value);
				break;
			case "BEGIN":
				if(value.equals("DAYLIGHT")||value.equals("STANDARD")){
					TimezoneProp tzProp = initTimezoneProp();
					if(value.equals("STANDARD")){
						StandardTime standard = StandardTime.of(tzProp);
						timezone.setStandard(standard);
					}
					else if(value.equals("DAYLIGHT")){
						Daylight daylight = Daylight.of(tzProp);
						timezone.setDaylight(daylight);
					}
					fileReadingIndex2--;
				}
				break;
			}
			fileReadingIndex2++;
		}
		fileReadingIndex1 = fileReadingIndex2;
	}
	
	private TimezoneProp initTimezoneProp(){
		fileReadingIndex3 = fileReadingIndex2+1;
		readSingleLine(fileReadingIndex3);
		
		TimezoneProp tzProp = new TimezoneProp();
		
		while((!attribute.equals("END"))&& !(value.equals("DAYLIGHT")||value.equals("STANDARD"))){
			readSingleLine(fileReadingIndex3);
			
			fileReadingIndex3 = checkSemicolons(fileReadingIndex3);
			
			switch (attribute) {
			case "TZNAME":
				tzProp.setTZName(value);
				break;
			case "TZOFFSETFROM":
				tzProp.setZtoffsetfrom(value);
				break;
			case "TZOFFSETTO":
				tzProp.setTzoffsetto(value);
				break;
			case "DTSTART":
				tzProp.setDtstart(value);
				break;
			}
			fileReadingIndex3++;
		}
		fileReadingIndex2 = fileReadingIndex3;
		
		return tzProp;
	}
	
	private Entry initEntry(){
		fileReadingIndex2 = fileReadingIndex1;
		readSingleLine(fileReadingIndex2);
		
		Entry entry = new Entry();
		
		while(!attribute.equals("END")&&!value.equals("VEVENT")||!attribute.equals("END")&&!value.equals("VTODO")){
			readSingleLine(fileReadingIndex2);
			
			fileReadingIndex2 = checkSemicolons(fileReadingIndex2);
			
			switch (attribute) {
			case "UID":
				entry.setUid(value);
				break;
			case "DTSTAMP":
				entry.setDtstamp(utcString2LocalDateTime(value));
				break;
			case "CLASS":
				entry.setClass(value);
				break;
			case "CREATED":
				entry.setCreated(value);
				break;
			case "DESCRIPTION":
				entry.setDescription(value);
				break;
			case "DTSTART":
				entry.setDtstart(utcString2LocalDateTime(value));
				break;
			case "DTEND":
				entry.setDtend(utcString2LocalDateTime(value));
				break;
			case "GEO":
				entry.setGeo(value);
				break;
			case "LOCATION":
				entry.setLocation(value);
				break;
			case "ORGANIZER":
				entry.setOrganizer(value);
				break;
			case "PRIORITY":
				entry.setPriority(value);
				break;
			case "RECURID":
				entry.setRecurid(value);
				break;
			case "STATUS":
				entry.setStatus(value);
				break;
			case "SUMMARY":
				entry.setSummary(value);
				break;
			case "DURATION":
				entry.setDuration(value);
				break;
			}
			fileReadingIndex2++;
		}
		
		return entry;
	}
	
	private void initAppointment(ICalAppointment appo){
		fileReadingIndex1++;
		readSingleLine(fileReadingIndex1);
		fileReadingIndex1 = checkSemicolons(fileReadingIndex1);
		
		while(!attribute.equals("END")&&!value.equals("VEVENT")){
			readSingleLine(fileReadingIndex1);
			
			switch (attribute) {
			case "TRANSP":
				appo.setTransp(value);
				break;
			case "URL":
				appo.setUrl(value);
				break;
			}
			fileReadingIndex1++;
		}
	}
	
	private void initTask(ICalTask pTask){
		fileReadingIndex1++;
		readSingleLine(fileReadingIndex1);
		fileReadingIndex1 = checkSemicolons(fileReadingIndex1);
		
		while(!attribute.equals("END")&&!value.equals("VTODO")){
			readSingleLine(fileReadingIndex1);
			
			switch (attribute) {
			case "COMPLETED":
				pTask.setCompleted(value);
				break;
			case "DUE":
				pTask.setDue(utcString2LocalDateTime(value));
				break;
			}
			fileReadingIndex1++;
		}
	}

	// Converts a timestring like "DTSTART:20060812T125900Z" to a LocalDateTime object
	private LocalDateTime utcString2LocalDateTime(String pUTC) {
		String date = pUTC.substring(0, 8);
		String time = pUTC.substring(9, 15);

		LocalDate ld = LocalDate.of(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(4, 6)), Integer.parseInt(date.substring(6)));
		LocalTime lt = LocalTime.of(Integer.parseInt(time.substring(0, 2)), Integer.parseInt(time.substring(2, 4)), Integer.parseInt(time.substring(4)));
		LocalDateTime ldt = LocalDateTime.of(ld, lt);

		return ldt;
	}
	
	private String[] extendArray(String[] pArray) {
		String[] temp = new String[pArray.length + 1];

		for (int i = 0; i < pArray.length; i++) {
			temp[i] = pArray[i];
		}
		pArray = temp;
		
		return pArray;
	}
	
	// Check if the calendar exists based on the calendar name
	private boolean checkCalendarExistence(String pName){
		Calendar[] cals = data.getCals();
		boolean found = false;
		
		for(int i=0; i<cals.length; i++){
			if(cals[i].getName().equals(pName)){
				found = true;
			}
		}
		return found;
	}
	
	// Get the calendar by comparing the names
	public Calendar getCalByName(String pName) {
		Calendar[] cals = data.getCals();
		Calendar cal = null;
		String calname;
		for (int i = 0; i < cals.length; i++) {
			calname = cals[i].getName();
			if (calname == pName) {
				cal = cals[i];
				break;
			}
		}
		return cal;
	}
	// Weitere Ideen: Mit Zyklen, Arbeitsschichten, Schule...
}

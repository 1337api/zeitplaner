create database if not exists `Zeitplaner`;

CREATE TABLE IF NOT EXISTS `Zeitplaner`.`Appointments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `DTstart` VARCHAR(45) NULL,
  `DTend` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `Zeitplaner`.`Calendars` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `calscale` VARCHAR(45) NULL,
  `method` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

DROP TABLE IF  EXISTS `Zeitplaner`.`Events`;

CREATE TABLE IF NOT EXISTS `Zeitplaner`.`Events` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idCalendar` INT NOT NULL,
  `uid` VARCHAR(45) NOT NULL,
  `dtstamp` VARCHAR(45) NOT NULL,
  `dtstart` VARCHAR(45) NOT NULL,
  `class` VARCHAR(45) NULL,
  `created` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `geo` VARCHAR(45) NULL,
  `last-mod` VARCHAR(45) NULL,
  `location` VARCHAR(45) NULL,
  `organizer` VARCHAR(45) NULL,
  `priority` VARCHAR(45) NULL,
  `seq` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `summary` VARCHAR(45) NULL,
  `transp` VARCHAR(45) NULL,
  `url` VARCHAR(45) NULL,
  `recurid` VARCHAR(45) NULL,
  `rrule` VARCHAR(45) NULL,
  `dtend` VARCHAR(45) NULL,
  `duration` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Events_Calendars_idx` (`idCalendar` ASC),
  CONSTRAINT `fk_Events_Calendars`
    FOREIGN KEY (`idCalendar`)
    REFERENCES `Zeitplaner`.`Calendars` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
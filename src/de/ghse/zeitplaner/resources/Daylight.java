package de.ghse.zeitplaner.resources;

public class Daylight extends TimezoneProp {
	  
	  // Anfang Attribute
	  // Ende Attribute
	  
	  public Daylight() {
	  }

	  // Anfang Methoden
	  public static Daylight of(TimezoneProp tzProp){
		  Daylight daylight = new Daylight();
		  
		  if(tzProp.getDtstart()!=null)
			  daylight.setDtstart(tzProp.getDtstart());
				
		  if(tzProp.getTZName()!=null)
			  daylight.setTZName(tzProp.getTZName());
		  
		  if(tzProp.getTzoffsetto()!=null)
			  daylight.setTzoffsetto(tzProp.getTzoffsetto());
		  
		  if(tzProp.getZtoffsetfrom()!=null)
			  daylight.setZtoffsetfrom(tzProp.getZtoffsetfrom());
		  
		return daylight;
	  }
	  // Ende Methoden
	} // end of Daylight
